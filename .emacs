(add-to-list 'load-path "~/.emacs.d/")
(add-to-list 'load-path "~/.emacs.d/auto-complete")
(add-to-list 'load-path "~/.emacs.d/jd-el")
(require 'xcscope)
(require 'google-maps)
(require 'rainbow-mode)
(require 'highlight-parentheses)
(require 'light-symbol)
(require 'auto-complete)

(set-default-font "-adobe-courier-medium-r-normal--14-100-100-100-m-90-iso8859-1")
(custom-set-variables
 '(c-basic-offset 2)
 '(c-offsets-alist (quote ((substatement-open . 0) (case-label . +))))
 '(inhibit-startup-screen t))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )

;; (setq hl-paren-colors
;;       '("orange1" "yellow1" "greenyellow" "green1"
;;         "springgreen1" "cyan1" "slateblue1" "magenta1" "purple"))


;; wanderlust
(autoload 'wl "wl" "Wanderlust" t)
(autoload 'wl-other-frame "wl" "Wanderlust on new frame." t)
(autoload 'wl-draft "wl-draft" "Write draft with Wanderlust." t)

;; IMAP
(setq elmo-imap4-default-server "imap.gmail.com")
(setq elmo-imap4-default-user "tomjnixon@googlemail.com") 
(setq elmo-imap4-default-authenticate-type 'clear) 
(setq elmo-imap4-default-port '993)
(setq elmo-imap4-default-stream-type 'ssl)

(setq elmo-imap4-use-modified-utf7 t) 

;; SMTP
(setq wl-smtp-connection-type 'starttls)
(setq wl-smtp-posting-port 587)
(setq wl-smtp-authenticate-type "plain")
(setq wl-smtp-posting-user "tomjnixon")
(setq wl-smtp-posting-server "smtp.gmail.com")
(setq wl-local-domain "gmail.com")

(setq wl-default-folder "%inbox")
(setq wl-default-spec "%")
(setq wl-draft-folder "%[Gmail]/Drafts") ; Gmail IMAP
(setq wl-trash-folder "%[Gmail]/Trash")

(setq wl-folder-check-async t) 

(setq elmo-imap4-use-modified-utf7 t)

(autoload 'wl-user-agent-compose "wl-draft" nil t)
(if (boundp 'mail-user-agent)
    (setq mail-user-agent 'wl-user-agent))
(if (fboundp 'define-mail-user-agent)
    (define-mail-user-agent
      'wl-user-agent
      'wl-user-agent-compose
      'wl-draft-send
      'wl-draft-kill
      'mail-send-hook))

;;(require 'emms-setup)
;;(require 'emms-player-mpd)
;;(emms-standard)
;;(emms-default-players)
;;(setq emms-player-mpd-server-name "localhost")
;;(setq emms-player-mpd-server-port "6600")
;;(add-to-list 'emms-info-functions 'emms-info-mpd)
(column-number-mode 1)

(set 'default-tab-width 8)

(setq indent-tabs-mode nil)

(autoload 'php-mode "php-mode.el" "Php mode." t)
(setq auto-mode-alist (append '(("/*.\.php[345]?$" . php-mode)) auto-mode-alist))

(autoload 'haskell-mode "haskell-mode/haskell-mode.el" "Haskell mode." t)
(setq auto-mode-alist (append '(("/*.\.hs$" . haskell-mode)) auto-mode-alist))

(require 'color-theme)
(color-theme-initialize)
(color-theme-dark-laptop)

(server-start)
(put 'upcase-region 'disabled nil)

(setq flyspell-sort-corrections nil)

(setq erc-log-channels-directory "~/.erc/logs/")
(setq erc-save-buffer-on-part t)

(defun execute-current ()
  (interactive)
  (executable-interpret (buffer-file-name)))

(global-set-key (kbd "C-x e") 'execute-current)


;;;;;;;;;;;;;;
(setq-default tab-width 4) ; or any other preferred value
(setq cua-auto-tabify-rectangles nil)
(defadvice align (around smart-tabs activate)
  (let ((indent-tabs-mode nil)) ad-do-it))
(defadvice align-regexp (around smart-tabs activate)
  (let ((indent-tabs-mode nil)) ad-do-it))
(defadvice indent-relative (around smart-tabs activate)
  (let ((indent-tabs-mode nil)) ad-do-it))
(defadvice indent-according-to-mode (around smart-tabs activate)
  (let ((indent-tabs-mode indent-tabs-mode))
    (if (memq indent-line-function
	      '(indent-relative
		indent-relative-maybe))
	(setq indent-tabs-mode nil))
    ad-do-it))
(defmacro smart-tabs-advice (function offset)
  (defvaralias offset 'tab-width)
  `(defadvice ,function (around smart-tabs activate)
     (cond
      (indent-tabs-mode
       (save-excursion
	 (beginning-of-line)
	 (while (looking-at "\t*\\( +\\)\t+")
	   (replace-match "" nil nil nil 1)))
       (setq tab-width tab-width)
       (let ((tab-width fill-column)
	     (,offset fill-column))
	 ad-do-it))
      (t
       ad-do-it))))
(smart-tabs-advice c-indent-line c-basic-offset)
(smart-tabs-advice c-indent-region c-basic-offset)


;;;;;;;;;;;;

;; (smart-tabs-advice python-indent-line-1 python-indent)
(add-hook 'python-mode-hook
	  (lambda ()
	    ;;(setq indent-tabs-mode nil)
	    ;;(setq tab-width (default-value 'tab-width))
		(auto-complete-mode)
		(define-key ac-mode-map (kbd "M-TAB") 'auto-complete)
		))


(setq erc-autojoin-channels-alist
	  '(("quakenet.org" "#uhc" "#mucs")))

(defun erc-connect ()
  (interactive)
  (erc :server "irc.quakenet.org" :port 6667 :nick "tomn" :full-name "Tom Nixon"))


(defun erc-start ()
  (interactive)
  (erc-connect)
  (sleep-for 6)

  (delete-other-windows)
  (split-window-vertically)
  (switch-to-buffer "#uhc")
  (other-window 1)
  (switch-to-buffer "#mucs")
)

(add-hook 'erc-mode-hook
		  'flyspell-mode)

(setq browse-url-browser-function 'browse-url-generic
	  browse-url-generic-program "google-chrome")

(global-set-key (kbd "<mouse-4>") (lambda () 
                                    (interactive)
                                    (scroll-down 2)))
(global-set-key (kbd "<mouse-5>") (lambda () 
                                    (interactive)
                                    (scroll-up 2)))

;; (xterm-mouse-mode)

(defun sprunge-region ()
  (interactive)
  (shell-command-on-region (region-beginning) 
						   (region-end) 
						   "curl -sF 'sprunge=<-' http://sprunge.us" 
						   "sprunge")
  (switch-to-buffer "sprunge"))

(add-hook 'lisp-mode-hook
		  (lambda ()
			(set (make-local-variable lisp-indent-function)
				 'common-lisp-indent-function)))