{-# LANGUAGE DeriveDataTypeable, FlexibleInstances, MultiParamTypeClasses #-}
import Control.Monad
import Data.Monoid
import Data.Ratio
import Graphics.X11.ExtraTypes.XF86
import qualified Data.Map as M
import qualified XMonad.Layout.IM as IM
import qualified XMonad.StackSet as W
import System.IO
import XMonad.Actions.CycleWS
import XMonad hiding (Point)
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.ComboP
import XMonad.Layout.Dishes
import XMonad.Layout.Grid
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.Named
import XMonad.Layout.NoBorders hiding (Never)
import XMonad.Layout.Reflect
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.TwoPane
import XMonad.Layout.WorkspaceDir
import XMonad.Prompt
import XMonad.Prompt.AppLauncher as AL
import XMonad.Prompt.Shell
import XMonad.Util.EZConfig
import XMonad.Util.Run(spawnPipe, runProcessWithInput)
--import XMonad.Actions.UpdatePointer
myModMask = mod4Mask
myTerminal = "xterm -bg black -fg white"

myDock = "killall trayer; trayer --SetPartialStrut true --height 8 --widthtype request --align right --edge top --transparent true --tint 0x000000"

myUrgencyConfig = urgencyConfig { suppressWhen = Visible }

xmobarStrip2 = xmobarStrip . concatMap (\x -> if (x == '}' || x == '{') then [] else [x])

spaces = [([d], [d]) | d <- ['a'..'z']]

main = do
    xmproc <- spawnPipe "xmobar"
    spawn myDock
    xmonad $ withUrgencyHookC NoUrgencyHook myUrgencyConfig $ defaultConfig
        { manageHook = manageHook defaultConfig <+> myManageHooks <+> manageDocks 
        , layoutHook = avoidStruts $ smartBorders $ workspaceDir "~" myLayout
        , logHook = fadeInactiveLogHook 0.8 >> (dynamicLogWithPP $ xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50 . xmobarStrip2
                        , ppUrgent = xmobarColor "yellow" "red" . xmobarStrip2
                        })
        , modMask = myModMask
        , terminal = myTerminal
        , handleEventHook = myEventHook
        , workspaces = workspaces defaultConfig ++ [s | (k, s) <- spaces]
        } `removeKeysP`
        [ "M-S-c" 
        ] `additionalKeysP` (
        [ ("C-<Print>", spawn "sleep 0.2; scrot -s")
        , ("<Print>", spawn "scrot")
        , ("M-d", spawn "disp --restore; xmonad --restart; xmodmap ~/.Xmodmap")
        , ("M-S-q", spawn "xmonad --recompile; xmonad --restart")
        , ("M-q", spawn "xmonad --restart")
        , ("M-<Return>", spawn myTerminal)
        , ("M-S-<Return>", spawn $ myTerminal ++ " -class xterm-top")
        , ("M-C-<Return>", spawn $ myTerminal ++ " -class xterm-float")
        , ("M-z", windows W.swapMaster)
        , ("M-c", kill)
        , ("M-S-h", sendMessage MirrorShrink)
        , ("M-S-l", sendMessage MirrorExpand)
        , ("M-S-f", sendMessage $ Toggle FULL)
        , ("M-S-m", sendMessage $ Toggle MIRROR)
        , ("M-r", shellPrompt myXPConfig)
        , ("M-<Esc>", toggleWS)
        , ("M-f", withFocused toggleFloating)
        , ("M-s", changeDir myXPConfig)
        , ("M-<Down>", spawn "mpc toggle")
        , ("M-<Up>", spawn "mpc stop")
        , ("M-<Left>", spawn "mpc prev")
        , ("M-<Right>", spawn "mpc next")
        , ("<XF86AudioPlay>", spawn "mpc toggle")
        , ("<XF86AudioPrev>", spawn "mpc prev")
        , ("<XF86AudioNext>", spawn "mpc next")
        , ("<XF86AudioRaiseVolume>", spawn "mpc volume +1")
        , ("<XF86AudioLowerVolume>", spawn "mpc volume -1")
        , ("<XF86AudioMute>", spawn "mpc volume 60")
        , ("M-<XF86AudioMute>", spawn "mpc volume 80")
        --, ("M-p", shellPrompt myXPConfig)
        ] ++ programKeys ++ spaceKeys) `additionalKeys`
        [ ((0, xF86XK_Launch7), spawn "shet /tom/toggle_desk")
        , ((0, xF86XK_Launch8), spawn "shet /tom/toggle_bedside")
        , ((0, xF86XK_Launch9), spawn "shet /tom/toggle_reading")
        ] `additionalMouseBindings`
        [ ((myModMask, 6), (\w -> prevWS))
        , ((myModMask, 7), (\w -> nextWS))
        ]
        
        where
          myManageHooks = composeAll 
                          [ isFullscreen --> (doF W.focusDown <+> doFullFloat)
                          , className =? "xterm-float" --> doCenterFloat
                          , className =? "Wine" --> doFloat
                          , className =? "Baby" --> doFullFloat
                          , className =? "Xmessage" --> doCenterFloat
                          ]
          -- myLayout = tall ||| Mirror tall ||| Full ||| Grid
          myLayout = withTerm ((named "Tall" tall) |||
                                 (named "Mirror Tall" $ Mirror tall) |||
                                 (named "Full" Full) |||
                                 (named "Grid" Grid) |||
                                 gimp2)
                   where
                     tall = ResizableTall 1 (5/100) (1/2) []
                     -- gimp = IM.withIM (0.11) (IM.Role "gimp-toolbox") $
                     --          reflectHoriz $
                     --          IM.withIM (0.15) (IM.Role "gimp-dock") Full
                     gimp2 = combineTwoP (TwoPane 0.03 0.11) (tabbed shrinkText defaultTheme)
                               (combineTwoP (TwoPane 0.03 0.85) 
                                            (tabbed shrinkText defaultTheme) 
                                            (tabbed shrinkText defaultTheme) 
                                            (Not $ Role "gimp-dock"))
                               (Role "gimp-toolbox")
                              
                     withTerm l = nameTail . nameTail . nameTail $
                                  Mirror $ 
                                  IM.withIM (0.15) (IM.ClassName "xterm-top") $ 
                                  Mirror l
                     
              
          myXPConfig = defaultXPConfig {position = Top}
          programs = [ ("c", "google-chrome")
                     , ("s", "sonata")
                     , ("g", "gimp")
                     , ("l", "xscreensaver-command -lock")
                     , ("f", "firefox")
                     , ("x", myTerminal ++ " -e ssh uni")
                     ]
          
          toggleFloating w = windows (\s -> if M.member w (W.floating s)
                                               then W.sink w s 
                                               else (W.float w (W.RationalRect 0.25 0.25 0.5 0.5) s))

          programKeys = [("M-p " ++ k, spawn c) | (k, c) <- programs]
          spaceKeys = [("M-x " ++ k, windows $ W.greedyView s) | (k, s) <- spaces]
                      ++ [("M-S-x " ++ k, windows $ W.shift s) | (k, s) <- spaces]
          myEventHook = handleEventHook defaultConfig-- +++ fullscreenEventHook
            where x +++ y = mappend x y
          
          -- data Mpc = Mpc
          -- instance XPrompt Mpc where
          --   showXPrompt Mpc = "mpc: "

          -- mpcPrompt c = mkXPrompt Mpc c (mkComplFunFromList' mpcCommands)
