# Check for an interactive session
[ -z "$PS1" ] && return

source shet_complete

alias ls='ls --color=auto'
alias ec="emacsclient -n"
alias et="emacsclient -t"
alias extdisp_seperate_above="xrandr --output VGA1 --auto --scale 1x1 --output LVDS1 --mode 1024x600 --pos 128x1024 --primary"
alias extdisp_same="xrandr --output VGA1 --mode 1024x768 --output LVDS1 --mode 1024x600"
alias extdisp_large="xrandr --output VGA1 --auto --output LVDS1 --mode 1024x600"
alias extdisp_off="xrandr --output VGA1 --off --output LVDS1 --mode 1024x600"

alias erc_start="emacs -nw -f erc-start"

#PS1='[\u@\h \W]\$ '
PS1='\[\e[0;32m\]\u\[\e[m\] \[\e[1;34m\]\W\[\e[m\] \[\e[0;32m\]£\[\e[m\] '
export SCALA_HOME="/usr/share/scala/"
export PATH="$PATH:/usr/share/java/jets3t/bin:~/bin:/home/tom/build/arm-2009q3/bin/"
export JETS3T_HOME="/usr/share/java/jets3t"
export EDITOR=nano
export BROWSER=google-chrome
export DE=gnome

export HISTSIZE=10000

export AWS_ACCESS_KEY_ID=AKIAJOT3ZC4JSFBWG5TA
export AWS_SECRET_ACCESS_KEY=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
export AWS_CALLING_FORMAT=SUBDOMAIN

export COMP10092=~/Documents/COMP10092
export PYTHONSTARTUP=~/.pythonrc

alias pbin="curl -F 'sprunge=<-' http://sprunge.us"
#export AWT_TOOLKIT=MToolkit webscarab
alias no="yes n"

export MPD_HOST="/home/tom/.mpd/socket"

export SHET_HOST="18sg.net"
export SHET_HTTP_URL="http://18sg.net:8080/"

function send_to_kindle()
{
	echo "$1" | ssh 18sg.net "cat > redirect"
}
